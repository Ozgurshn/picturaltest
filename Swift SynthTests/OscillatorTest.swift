//
//  Swift_SynthTests.swift
//  Swift SynthTests
//
//  Created by ozgur on 12/20/19.
//  Copyright © 2019 Grant Emerson. All rights reserved.
//

import XCTest

class OscillatorTest: XCTestCase {
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testSine() {
        let sine = Oscillator.sine
        let result = sine(1)
        XCTAssertEqual(result, -0.000216799643)
    }
    
    func testTriangle() {
        let triangle = Oscillator.triangle
        let result = triangle(1)
        XCTAssertEqual(result, 0.0000000000000854871728)
    }
    
    func testSawtooth() {
        let sawtooth = Oscillator.sawtooth
        let result = sawtooth(1)
        XCTAssertEqual(result, -0.999972105)
    }
    
    func testSquare() {
        let square = Oscillator.square
        let result = square(1)
        XCTAssertEqual(result, 1)
    }
    
    func testWhiteNoise() {

        let whiteNoise = Oscillator.whiteNoise
        let result = whiteNoise(1)/Oscillator.amplitude
        XCTAssertTrue(result<1 &&  result > -1)
    }

    
}
